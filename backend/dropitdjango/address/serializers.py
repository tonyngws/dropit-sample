from dropitdjango.address.models import Address
from rest_framework import serializers


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = [
            'id',
            'first_name',
            'last_name',
            'street_address_1',
            'street_address_2',
            'city',
            'postal_code',
            'country']
