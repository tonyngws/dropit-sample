from django.apps import AppConfig


class ConsignmentsConfig(AppConfig):
    name = 'dropitdjango.consignments'
