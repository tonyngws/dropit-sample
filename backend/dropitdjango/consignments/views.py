from django.shortcuts import render
from dropitdjango.consignments.models import Consignments
from dropitdjango.consignments.serializers import ConsignmentsSerializer
from rest_framework import viewsets

# Create your views here.


class ConsignmentViewSet(viewsets.ModelViewSet):
    queryset = Consignments.objects.all()
    serializer_class = ConsignmentsSerializer
