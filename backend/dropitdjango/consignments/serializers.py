from dropitdjango.address.models import Address
from dropitdjango.address.serializers import AddressSerializer
from dropitdjango.consignments.models import Consignments
from rest_framework import serializers


class ConsignmentsSerializer(serializers.ModelSerializer):
    address = AddressSerializer()

    class Meta:
        model = Consignments
        fields = [
            'id',
            'created_at',
            'updated_at',
            'delivery_type',
            'address',
            'price']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def create(self, validated_data):
        address = validated_data.pop('address')
        return Consignments.objects.create(
            **validated_data, 
            address=Address.objects.create(
                **address
            )
        )
