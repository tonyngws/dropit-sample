from dropitdjango.consignments import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('', views.ConsignmentViewSet)
