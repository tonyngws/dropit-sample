from django.db import models
from django.utils.translation import gettext_lazy as _
from dropitdjango.address.models import Address

# Create your models here.


class Consignments(models.Model):

    class Meta:
        ordering = ['id']

    class DeliveryType(models.TextChoices):
        SAME_DAY_DELIVERY = 'same day delivery', _('Same Day Delivery')
        EXPRESS_DELIVERY = 'express delivery', _('Express Delivery')
        NORMAL_DELIVERY = 'normal delivery', _('Normal Delivery')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    delivery_type = models.CharField(
        max_length=255,
        choices=DeliveryType.choices,
        default=DeliveryType.NORMAL_DELIVERY)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    price = models.PositiveIntegerField(default=0)
