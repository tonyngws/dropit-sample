import React from 'react';

import { ColumnsType } from 'antd/lib/table';
import { useRouter } from 'next/router';
import { Card, Spin, Table } from 'antd';

import { IConsignment } from 'types';

interface Props {
  consignments: IConsignment[];
}

const consignmentsColumns: ColumnsType<any> = [
  {
    title: 'Created',
    dataIndex: 'created_at',
    key: 'created',
  },
  {
    title: 'Delivery Type',
    dataIndex: 'delivery_type',
    key: 'type',
  },
  {
    title: 'First Name',
    dataIndex: 'first_name',
    key: 'first_name',
  },
  {
    title: 'Last Name',
    dataIndex: 'last_name',
    key: 'last_name',
  },
];

const ConsignmentList: React.FC<Props> = ({ consignments }) => {
  const router = useRouter();

  if (consignments) {
    return (
      <Card>
        <Table
          onRow={(record): any => {
            return {
              onClick: (): void => {
                router.push('/consignment/[id]', `/consignment/${record.id}`);
              },
            };
          }}
          dataSource={consignments.map(
            ({ id, created_at, delivery_type, address }: IConsignment) => ({
              key: id,
              id,
              created_at,
              delivery_type,
              first_name: address.first_name,
              last_name: address.last_name,
            })
          )}
          columns={consignmentsColumns}
        />
      </Card>
    );
  } else {
    return (
      <Card>
        <Spin>
          <Table />
        </Spin>
      </Card>
    );
  }
};

export default ConsignmentList;
