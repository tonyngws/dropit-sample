import React, { useEffect, useRef } from 'react';

import { FormInstance } from 'antd/lib/form';
import { Store } from 'antd/lib/form/interface';
import { Button, Form, Input, Modal, Select } from 'antd';

interface Props {
  visible: boolean;
  hideModal: () => void;
  onFinish: (values: Store) => void;
  error: string;
}

const ConsignmentForm: React.FC<Props> = ({
  visible,
  hideModal,
  onFinish,
  error,
}: Props) => {
  const formRef = useRef<FormInstance>(null);

  useEffect(() => {
    if (formRef.current) formRef.current.resetFields();
  }, [visible]);

  return (
    <Modal
      visible={visible}
      title="Create Consignment"
      onCancel={hideModal}
      footer={null}
    >
      <Form onFinish={(values: Store): void => onFinish(values)} ref={formRef}>
        {error && <p className="error">{error}</p>}
        <Form.Item name="delivery_type">
          <Select size="large" placeholder="Delivery Type">
            <Select.Option value="same day delivery">
              Same Day Delivery
            </Select.Option>
            <Select.Option value="express delivery">
              Express Delivery
            </Select.Option>
            <Select.Option value="normal delivery">
              Normal Delivery
            </Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="first_name"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please input your first name',
              whitespace: true,
            },
          ]}
        >
          <Input size="large" placeholder="First Name" />
        </Form.Item>
        <Form.Item
          name="last_name"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please input your last name',
              whitespace: true,
            },
          ]}
        >
          <Input size="large" placeholder="Last Name" />
        </Form.Item>
        <Form.Item
          name="street_address_1"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please enter address line 1',
              whitespace: true,
            },
          ]}
        >
          <Input size="large" placeholder="Address line 1" />
        </Form.Item>
        <Form.Item name="street_address_2" hasFeedback>
          <Input size="large" placeholder="Address line 2" />
        </Form.Item>
        <Form.Item
          name="city"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please input city',
              whitespace: true,
            },
          ]}
        >
          <Input size="large" placeholder="City" />
        </Form.Item>
        <Form.Item
          name="postal_code"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please input postal code',
              whitespace: true,
            },
          ]}
        >
          <Input size="large" placeholder="Postal Code" />
        </Form.Item>
        <Form.Item
          name="country"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please input country',
              whitespace: true,
            },
          ]}
        >
          <Input size="large" placeholder="Country" />
        </Form.Item>
        <Form.Item>
          <Button size="large" type="primary" htmlType="submit">
            Create
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ConsignmentForm;
