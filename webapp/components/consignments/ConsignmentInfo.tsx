import React, { Fragment } from 'react';

import { Card, Descriptions } from 'antd';

import { IConsignment } from 'types';

interface Props {
  consignment: IConsignment | undefined;
}

const ConsignmentInfo: React.FC<Props> = ({ consignment }: Props) => {
  return (
    <Card>
      <Descriptions
        title={`Consignment ${consignment ? consignment.id : ''}`}
        bordered
      >
        {consignment && (
          <Fragment>
            <Descriptions.Item label="ID">{consignment.id}</Descriptions.Item>
            <Descriptions.Item label="Created">
              {consignment.created_at}
            </Descriptions.Item>
            <Descriptions.Item label="Last Updated">
              {consignment.updated_at}
            </Descriptions.Item>
            <Descriptions.Item label="Type">
              {consignment.delivery_type}
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              {`${consignment.address.first_name} ${consignment.address.last_name}`}
              <br />
              {consignment.address.street_address_1}
              <br />
              {consignment.address.street_address_2 && (
                <>
                  {consignment.address.street_address_2}
                  <br />
                </>
              )}
              {`${consignment.address.postal_code}, ${consignment.address.city}`}
              <br />
              {consignment.address.country}
            </Descriptions.Item>
            <Descriptions.Item label="Price">
              {`RM ${(consignment.price / 100).toFixed(2)}`}
            </Descriptions.Item>
          </Fragment>
        )}
      </Descriptions>
    </Card>
  );
};

export default ConsignmentInfo;
