import { DeliveryType, IConsignment, Paginator } from 'types';

export type ConsignmentResponse = IConsignment;

export type ConsignmentsResponse = Paginator<IConsignment>;

export interface ConsignmentRequestData {
  delivery_type: DeliveryType;
  address: {
    first_name: string;
    last_name: string;
    street_address_1: string;
    street_address_2?: string;
    city: string;
    postal_code: string;
    country: string;
  };
}
