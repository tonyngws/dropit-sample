import axios, { AxiosInstance } from 'axios';

import { ConsignmentApiMixin } from './api/consignment';
import applyMixins from '../utils/applyMixins';

export class BaseApiService {
  private headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  protected client: AxiosInstance;

  public constructor() {
    this.client = axios.create({
      baseURL: process.env.NEXT_PUBLIC_API_URL || 'https://localhost:8000/',
      headers: this.headers,
    });
  }
}

export const ApiService = applyMixins(BaseApiService, [ConsignmentApiMixin]);
