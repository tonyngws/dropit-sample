import { AxiosResponse } from 'axios';

import { BaseApiService } from '../api-service';
import { AnyClass, MixinConstructor } from 'types';
import {
  ConsignmentRequestData,
  ConsignmentResponse,
  ConsignmentsResponse,
} from '../types';

export function ConsignmentApiMixin<
  TBase extends MixinConstructor<BaseApiService>
>(Base: TBase): AnyClass {
  return class extends Base {
    public async getConsignments(): Promise<
      AxiosResponse<ConsignmentsResponse>
    > {
      return await this.client.get('consignments/');
    }

    public async getConsignment(
      id: string
    ): Promise<AxiosResponse<ConsignmentResponse>> {
      return await this.client.get(`consignments/${id}/`);
    }

    public async createConsignment(
      data: ConsignmentRequestData
    ): Promise<AxiosResponse<ConsignmentResponse>> {
      return await this.client.post('consignments/', data);
    }

    public async deleteConsignment(id: string): Promise<AxiosResponse> {
      return await this.client.delete(`consignments/${id}/`);
    }
  };
}
