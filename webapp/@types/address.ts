export interface IAddress {
  id: string;
  first_name?: string;
  last_name?: string;
  street_address_1?: string;
  street_address_2?: string;
  city?: string;
  postal_code?: string;
  country?: string;
}
