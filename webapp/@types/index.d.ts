declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';
declare module '*.gif';

declare module 'HTMLCallbackType' {
  import { RadioChangeEvent } from 'antd/lib/radio';
  export type onChangeType = (
    e:
      | RadioChangeEvent
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
  ) => void;
}

declare module 'ReactCustomType' {
  import React from 'react';
  export type HOC = <T extends any>(
    Component: React.ComponentType<T>
  ) => React.ComponentType<T>;
}

declare module 'types' {
  export type AnyClass = { new (): any };
  export type MixinConstructor<T = obj> = new (...args: any[]) => T;
  export type Mixin<T = obj> = (Base: MixinConstructor<T>) => { new (): any };

  export { IConsignment, DeliveryType } from './consignment';
  export { Paginator } from './paginator';
}
