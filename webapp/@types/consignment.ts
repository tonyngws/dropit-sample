import { IAddress } from './address';

export enum DeliveryType {
  SameDay = 'same day delivery',
  Express = 'express delivery',
  Normal = 'normal delivery',
}

export interface IConsignment {
  id: string;
  created_at: string;
  updated_at: string;
  delivery_type: DeliveryType;
  address: IAddress;
  price: number;
}
