export interface Paginator<T> {
  next?: string;
  previous?: string;
  count: number;
  results: T[];
}
