import Link from 'next/link';
import { SiderMenuProps } from '@ant-design/pro-layout/lib/SiderMenu/SiderMenu';
import { SmileOutlined } from '@ant-design/icons';
import dynamic from 'next/dynamic';
import { MenuDataItem, Route } from '@ant-design/pro-layout/lib/typings';

const ProLayout = dynamic(() => import('@ant-design/pro-layout'), {
  ssr: false,
});

const ROUTES: Route = {
  path: '/',
  routes: [
    {
      path: '/',
      name: 'Consignment',
      icon: <SmileOutlined />,
    },
  ],
};

const menuHeaderRender = (
  logo: React.ReactNode,
  title: React.ReactNode,
  props?: SiderMenuProps
): React.ReactNode => (
  <Link prefetch href="/">
    <a>
      {logo}
      {/* eslint-disable-next-line react/prop-types */}
      {!props?.collapsed && title}
    </a>
  </Link>
);

const menuItemRender = (
  options: MenuDataItem,
  element: React.ReactNode
): React.ReactNode => (
  <Link prefetch href={options.path != undefined ? options.path : ''}>
    <a>{element}</a>
  </Link>
);

interface Props {
  children: React.ReactNode;
}

const Main: React.FC<Props> = ({ children }: Props): React.ReactElement => {
  return (
    <ProLayout
      style={{ minHeight: '100vh' }}
      route={ROUTES}
      menuItemRender={menuItemRender}
      menuHeaderRender={menuHeaderRender}
    >
      {children}
    </ProLayout>
  );
};

export default Main;
