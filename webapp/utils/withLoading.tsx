/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React from 'react';

import { Spin } from 'antd';

interface Props {
  loading: boolean;
}

const withLoading = <T extends object>(
  Component: React.ComponentType<T>
): React.FC<T & Props> => ({ loading, ...props }): React.ReactElement<T> =>
  loading ? (
    <Spin>
      <Component {...(props as T)} />
    </Spin>
  ) : (
    <Component {...(props as T)} />
  );

export default withLoading;
