import { AnyClass, Mixin } from 'types';

export default function applyMixins(
  derivedCtor: AnyClass,
  baseCtors: Array<Mixin<any>>
): AnyClass {
  const baseCtor = baseCtors.pop();

  if (baseCtor == undefined) return derivedCtor;

  return applyMixins(baseCtor(derivedCtor), baseCtors);
}
