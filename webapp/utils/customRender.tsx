import React from 'react';

import Link from 'next/link';
import { Route } from 'antd/lib/breadcrumb/Breadcrumb';

export const breadcrumbRender: (
  route: Route,
  params: any,
  routes: Array<Route>,
  paths: Array<string>
) => React.ReactNode = (route) => {
  return route.path ? (
    <span>
      <Link prefetch href={route.path}>
        <a>{route.breadcrumbName}</a>
      </Link>
    </span>
  ) : (
    <span>{route.breadcrumbName}</span>
  );
};
