import React, { useEffect, useState } from 'react';

import { AxiosResponse } from 'axios';
import { Route } from 'antd/lib/breadcrumb/Breadcrumb';
import { useRouter } from 'next/router';
import { Button, Modal, PageHeader, message } from 'antd';

import ConsignmentInfo from '../../components/consignments/ConsignmentInfo';
import { ConsignmentResponse } from '../../api/types';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { IConsignment } from 'types';
import MainLayout from '../../layouts/main';
import { ServiceResolver } from '../../api';
import { breadcrumbRender } from '../../utils/customRender';
import withLoading from '../../utils/withLoading';

const ConsignmentInfoWithLoading = withLoading(ConsignmentInfo);

const routes = (consignment: IConsignment | undefined): Route[] => {
  const routes = [
    {
      path: '/',
      breadcrumbName: 'Home',
    },
    {
      path: consignment ? `/consignment/${consignment.id}` : '',
      breadcrumbName: consignment ? consignment.id : 'Loading',
    },
  ];

  return routes;
};

const Consignment: React.FC = () => {
  const router = useRouter();
  const api = ServiceResolver.apiResolver();
  const id = Number(router.query.id);

  const [consignment, setConsignment] = useState<IConsignment | undefined>(
    undefined
  );
  const [loading, setLoading] = useState<boolean>(false);

  const laodInitialData = async (): Promise<void> => {
    console.log(id);
    if (!id || Array.isArray(id)) return;
    console.log('query');

    setLoading(true);
    try {
      const {
        data,
      }: AxiosResponse<ConsignmentResponse> = await api.getConsignment(id);
      setConsignment(data);
    } catch (err) {
      message.error(err.message);
    }
    setLoading(false);
  };

  useEffect(() => {
    laodInitialData();
  }, [id]);

  const onRemoveConsignment = async (): Promise<void> => {
    setLoading(true);
    try {
      await api.deleteConsignment(id);
      router.push('/');
    } catch (err) {
      message.error(err.message);
    }
    setLoading(false);
  };

  return (
    <MainLayout>
      <PageHeader
        backIcon={false}
        title="Consignments"
        breadcrumb={{
          routes: routes(consignment),
          itemRender: breadcrumbRender,
        }}
        extra={
          <Button
            danger
            type="primary"
            onClick={(): void => {
              Modal.confirm({
                title: 'Delete?',
                icon: <ExclamationCircleOutlined />,
                content: `Are you sure you want to delete consignment ${consignment.id}?`,
                okText: 'Delete',
                cancelText: 'Cancel',
                onOk: onRemoveConsignment,
              });
            }}
          >
            Delete
          </Button>
        }
      />
      <ConsignmentInfoWithLoading consignment={consignment} loading={loading} />
    </MainLayout>
  );
};

export default Consignment;
