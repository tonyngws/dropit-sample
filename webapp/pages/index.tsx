import { useEffect, useState } from 'react';

import { AxiosResponse } from 'axios';
import { Route } from 'antd/lib/breadcrumb/Breadcrumb';
import { Button, PageHeader, message } from 'antd';

import ConsignmentForm from '../components/consignments/modal/ConsignmentForm';
import ConsignmentList from '../components/consignments/ConsignmentList';
import { ConsignmentsResponse } from '../api/types';
import { IConsignment } from 'types';
import MainLayout from '../layouts/main';
import { ServiceResolver } from '../api';
import { Store } from 'antd/es/form/interface';
import { breadcrumbRender } from '../utils/customRender';
import withLoading from '../utils/withLoading';

const ConsignmentListWithLoading = withLoading(ConsignmentList);

const routes: Route[] = [
  {
    path: '/',
    breadcrumbName: 'Consignment',
  },
];

const Home: React.FC = () => {
  const api = ServiceResolver.apiResolver();
  const [consignments, setConsignments] = useState<IConsignment[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [showCreateModal, setShowCreateModal] = useState<boolean>(false);
  const [error, setError] = useState<string>('');

  const loadInitialData = async (): Promise<void> => {
    setLoading(true);
    try {
      const {
        data,
      }: AxiosResponse<ConsignmentsResponse> = await api.getConsignments();
      setConsignments(data.results);
    } catch (err) {
      message.error(err.message);
    }
    setLoading(false);
  };

  useEffect(() => {
    loadInitialData();
  }, []);

  const onConsignmentCreate = async ({
    delivery_type,
    first_name,
    last_name,
    street_address_1,
    street_address_2,
    postal_code,
    city,
    country,
  }: Store): Promise<void> => {
    setLoading(true);
    try {
      const { data } = await api.createConsignment({
        delivery_type,
        address: {
          first_name,
          last_name,
          street_address_1,
          street_address_2,
          postal_code,
          city,
          country,
        },
      });
      setConsignments((consignments) => consignments.concat(data));
      setShowCreateModal(false);
    } catch (err) {
      setError(err.message);
    }
    setLoading(false);
  };

  return (
    <MainLayout>
      <PageHeader
        backIcon={false}
        title="Consignments"
        breadcrumb={{
          routes,
          itemRender: breadcrumbRender,
        }}
        extra={
          <Button type="primary" onClick={(): void => setShowCreateModal(true)}>
            Create
          </Button>
        }
      />
      <ConsignmentForm
        visible={showCreateModal}
        hideModal={(): void => setShowCreateModal(false)}
        onFinish={onConsignmentCreate}
        error={error}
      />
      <ConsignmentListWithLoading
        loading={loading}
        consignments={consignments}
      />
    </MainLayout>
  );
};

export default Home;
